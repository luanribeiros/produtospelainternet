import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { FacebookCircle } from "@styled-icons/boxicons-logos/FacebookCircle"
import Logo from "../images/logo-only.svg"

const IndexPage = () => {
  return (
    <Layout>
      <SEO title="Home" />
      <img src={Logo} alt="" />
      <h1>
        Você vai ver aqui <span>Análises e Reviews Detalhados</span> de diversos
        <span> Produtos</span> pela <span>internet</span>, Tanto Produtos
        <span> Físicos</span> como <span>Digitais!</span>
        <div>
          <a
            href="https://www.facebook.com/produtospelainternet"
            target="_blank"
            alt="Ícone do Facebook"
          >
            <FacebookCircle />
          </a>
        </div>
      </h1>
    </Layout>
  )
}

export default IndexPage
